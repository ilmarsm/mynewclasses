package com.Ilmars;

public class Main {

    public static void main(String[] args) {

        Doors leftFront = new Doors();
        leftFront.name = "Left front ";
        leftFront.close();
        leftFront.doorStatus();

        Doors leftRear = new Doors();
        leftRear.name = "Left rear ";
        leftRear.close();
        leftRear.doorStatus();

        Doors rightFront = new Doors();
        rightFront.name = "Right front ";
        rightFront.doorStatus();

        Doors rightRear = new Doors();
        rightRear.name = "Right rear ";
        rightRear.doorStatus();

    }
}
