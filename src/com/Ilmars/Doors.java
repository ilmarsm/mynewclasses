package com.Ilmars;

public class Doors {
    String name;
    boolean isOpen;

    void open() {
            isOpen = true;
    }
    void close() {
        isOpen = false;
    }
    void doorStatus() {
        if (isOpen) {
            System.out.println(name + " door is open.");
        } else {
            System.out.println(name + " door is closed.");
        }
    }
}
